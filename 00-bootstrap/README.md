# Babylon.js typescript bootstrap project

This is a project setup for babylonjs with:

* Typescript support
* Using webpack 
* Hot load with webpack-dev-server

## Install

    $ git clone https://gitlab.com/fedelsoy/babylonjs-learn.git
    
    $ cd babylonjs-learn/00-bootstrap-ts
    $ npm install
    $ npm run dev
    $ browse http://localhost:9000

You must see the 'Basic Scene' example from playground https://playground.babylonjs.com/

## Edit

You can edit *src/main.ts* and the browser will reload automaticly.



